
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['money'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
	
		data.type = 'input';
	
	return [

		window.el['__tpl~el_input'].tpl.bind(this)(_, d, data, tpl),

	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent)
		{
			doAfterLoad.push(function(){
				realParent.find('input[code='+data.code+']').mask('000 000 000 000 000', {reverse: true});
			});
			
			this.save = function($e, data, callback){
				data.value = data.value.replace(/\D/g,'');
				callback( true, data );
			}
			if(!data.onSave){
				const flatTpl = tpl.flat(Infinity);
				const el = flatTpl[flatTpl.indexOf('input')+1];
				if(!el.onSave) el.onSave = 'saveTheme';
			}
		}
	},
	script: ()=>{
		
		window.toMoney = function(val, f){
			const valArr = val.split('.');
			return valArr[0]+'' 
				? ((valArr[0]*1).toFixed(f||0)+'')
					.replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1' + String.fromCharCode(160))+(valArr[1]?'.'+valArr[1]:'')
				: '';
		}
	},
}

exports['money+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl~el_money'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_money'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	}
}

exports['money-'] = 
exports['money--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
	
		if(data.value != undefined) data.value = toMoney(data.value);
	
	return [

		window.el['__tpl~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_money'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	}
}