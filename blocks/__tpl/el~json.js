
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['json'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			realParent.attr('f-'+(data.name||'').replace(/\./g, '_'), JSON.stringify({code: data.code, value: data.value}));
			realParent.data(data.name, data.value); // как то странно заработало с первого раза...
		},
	},
}

exports['json+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [
		window.el['__tpl~el_json'].tpl.bind(this)(_, d, data, tpl),
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			realParent.attr('code', data.code);
			window.el['__tpl~el_json'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['json-'] =
exports['json--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [
		window.el['__tpl~el_json'].tpl.bind(this)(_, d, data, tpl),
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_json'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}