
exports.f = function(conn, data, callback){

	data.msg.q = (data.msg.q||'').trim();
	
	if(!data.msg.q){
		callback({status: 'err', err: 'Строка поиска не может быть пустой'});
	}else{
		
		var res = {
			return: {results: []},
		};
		
		(SYS.get(data.msg,  'config.prepareSQL')||(()=>{}))(res);
		
		DB.select(conn, res, (err, selectData)=>{ if(!selectData.length){ callback(res.return) }else{
			selectData = selectData.filter(_=>_._id && _._id!==true);

			async.parallel((SYS.get(data.msg, 'config.dataCol')||[]).map(col=>((cb)=>{
				res[col.col] = {};
				
				conn.db.collection(col.col).find(
					{_id: {$in: selectData.map(_=>ObjectId(_[col.id||'_id']))}}, 
					new Map(col.fields.map(_=>[_,1]))
				).toArray((err, list)=>{
					(list||[]).forEach(_=>{ res[col.col][_._id] = _ });
					cb();
				});
			})), (err)=>{

				res.return.results = selectData.map((_)=>({
					db: _, id: _._id, text: _.text||'', context: _.context||'',
					icon: _.icon||('/blocks/'+SYS.get(data.msg,  'config.col')+'/static/img/icon.png'),
				}));
				
				(SYS.get(data.msg,  'config.prepareResult')||(()=>{}))(res);
				
				res.return.results.forEach(_=>{ delete _.db });
				
				callback(res.return);
			});
			
		}});
	}
}