

	var t = 10;
	var fail = {form:'#formContent', query: {form: '__tpl/lst_editor~main', container: "formContent"}};

	var lst = [{
		v: 'info', l: 'info',
		check: {search: '#formContent #lstList', fail: fail},
		text: [{
			t: 'Это редактор справочников. С его помощью можно отредактировать все справочники, используемые в системе.',
			controls: {
				'Продолжай': function(){ window.setTutorialComplete() },
				exit: {t: 'Я разберусь, спасибо', f: function(){ window.setTutorialComplete({endTutorial: true}) }},
			},
		}],
		position: 'bottomRight',
	},{
		v: 'list', l: 'list',
		check: {search: '#formContent #lstList', fail: fail},
		action: function(callback){
			$('#formContent #lstList [lstcode="__tpl/lst_editor~tutorial_info"]').trigger('click');
			callback();
		},
		text: [{
			active: {l: '#formContent #lstList', css: {'background-color': 'white'}},
			t: 'В данном списке перечислены все доступные для редактирования справочники. Для перехода к форме редактирования, нужно нажать на соответствующий элемент списка.',
			controls: {
				'Продолжай': function(){ window.setTutorialComplete() },
			},
		}],
		position: 'bottomRight',
	},{
		v: 'editor', l: 'editor',
		check: {search: '#formContent #lstEditor', fail: fail},
		action: function(callback){
			$('#formContent #lstEditor .jsoneditor-expand-all').trigger('click');
			callback();
		},
		text: [{
			active: {l: '#formContent #lstEditor', css: {'background-color': 'white'}},
			t: 'Редактор имеет интуитивно понятный интерфейс и позволяет как добавлять новые записи, так и редактировать существующие. По кнопке "Дополнительно" каждый режающий может узнать подробности о причинах выбора такого формата редактора.',
			controls: {
				exit: {t: 'Спасибо', f: function(){ window.setTutorialComplete({endTutorial: true}) }},
				'Дополнительно': function(){ window.setTutorialComplete() },
			},
		}],
		position: 'bottomLeft',
	},{
		v: 'editor_extra', l: 'editor_extra',
		check: {search: '#formContent #lstEditor', fail: fail},
		text: [{
			t: `Данный редактор удобен потому что:
- применим не только для "плоских" таблиц, но и для справочников со сложными вложенными json-структурами
- поддерживает javascript-справочники, используемые для построения интерфейсов, которые могут содержать в себе функции`,
			controls: {
				exit: {t: 'Ясно, понятно', f: function(){ window.setTutorialComplete({endTutorial: true}) }},
			},
		}],
		position: 'bottomLeft',
	}];
if(typeof exports != 'undefined') exports.lst = lst;
if(typeof window != 'undefined') window.LST['__tpl/lst_editor~tutorial_info'] = lst;