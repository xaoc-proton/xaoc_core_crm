
exports.f = function(msg)
{
	const file = SYS.getFileProjectData('lst', this.actionField.parent.filter.code);
	const path = !file.path.includes('_ver.js') ? file.path.replace('.js','__user_ver.js') : file.path;

	const funcList = {};
	
	funcFromString = (o)=>{
		for( let [key, value] of Object.entries(o) )
		{
			if(typeof value == 'string' && value.indexOf('function(') == 0)
			{
				const hash = md5(value.toString());
				funcList[hash] = value.toString();
				o[key] = hash;
			}
			else if(typeof value == 'object') funcFromString(value);
		}
	}
	funcFromString(msg.data);
	
	let lst = `
		var lst = ${ JSON.stringify(msg.data) };
		if(typeof exports != 'undefined') exports.lst = lst;
		if(typeof window != 'undefined') window.LST['${file.name.replace('.js','')}'] = lst;
	`;
	for( let [key, value] of Object.entries(funcList) )
		lst = lst.replace(new RegExp('"'+key+'"', 'g'), value);
		
	fs.writeFileSync(path, lst);

	SYS.updateLST({prepare: {key: file.name.replace('.js','')+'__user_ver', link: path}});
	
	return OK();
}