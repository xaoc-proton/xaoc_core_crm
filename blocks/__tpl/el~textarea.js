
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['textarea'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
	
		data.text = data.value+'';
	
	return [

		['div',{class: data.class+" "},[
			['label',{},[
				['span',{text: data.label}],
			]],
			['textarea', data],
		]],

	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			(window.resizeTextarea ? doAfterLoad : afterAllLoaded).push(function(){
				window.resizeTextarea(realParent.find('textarea[code='+data.code+']'));
			});
		}
	},
	script: ()=>{
		
		window.resizeTextarea = function($e){
			$e.css('height', 'auto');
			var h = $e.prop('scrollHeight') + 5, min_h = parseInt($e.css('min-height'));
			if(h < min_h) h = min_h;
			$e.css('height', h + 'px');
			$e = $e.parent();
			$e.css('height', 'auto');
			h = $e.prop('scrollHeight') + 5;
			if(h < min_h) h = min_h;
			$e.css('height', h + 'px');
		}
		
		window.resizeAllTextarea = function($e){
			$e.find('textarea').each(function(){ window.resizeTextarea( $(this) ) });
		}
		
		$(document).off('change', 'textarea');
		$(document).on('change', 'textarea', function(){
			window.resizeTextarea( $(this) );
		});
	},
}

exports['textarea+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl~el_textarea'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_textarea'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	}
}

exports['textarea-'] = 
exports['textarea--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		['div',{class: data.class+" "},[
			['label',{},[
				['span',{text: data.label}],
			]],
			['pre', {text: data.value, style: 'background: none;'}],
		]],
		
	]},
}