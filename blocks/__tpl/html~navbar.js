
exports.tpl = (_, d)=>{ return [

	["nav",{"class": "navbar navbar-static-top bNavbar","style": "margin-bottom: 0"},[

		["div",{class: "navbar-header *css*", style: ()=>{/*css
		
			@media (max-width: 768px){
				.*css* {
					z-index: 9999;
				}
			}
		
		css*/}},[
			["div",{class: "navbar-minimalize minimalize-styl-2 btn btn-primary"},[
				["i",{class: "fa fa-bars"}]
			]],
			/*["form",{role: "search",class: "navbar-form-custom",action: "search_results.html"},[
				["div",{class: "form-group"},[
					["input",{type: "text",placeholder: "Search for something...",class: "form-control",name: "top-search",id: "top-search"}]
				]]
			]]*/
		]],
		
		["ul",{"class": "nav navbar-top-links navbar-right *css*", style: ()=>{/*css
		
			@media (max-width: 768px){
				.*css* > li > a {
					padding: 4px 20px;
					min-height: 20px;
				}
				.body-small .*css* > .logout {
					position: absolute;
					top: 10px;
					right: 0px;
				}
			}

		css*/}},[
		
			["li",{class: "dropdown *css*", style: ()=>{/*css
				.*css*.open > .dropdown-menu {
					display: flex;
					max-height: 400px;
					overflow: auto;
				}
				.*css* {
					white-space: nowrap;
				}
				.*css* > a {
					text-align: right;
				}
				.*css* > .dropdown-menu > div {
					padding: 10px;
				}
				.*css* > .dropdown-menu > div > div {
					padding: 8px 4px;
				}
				@media (max-width: 768px){
					.*css* > .dropdown-menu {
						width: 100%;
					}
				}
			css*/}},[
				_.html('user~roles', _, d),
			]],
			
			["li",{class: 'profile-link'},[
				["a",{"href": "#", query: '{"form":"user~profile", "container":"formContent"}'},[
					["i",{"class": "fas fa-user-circle"}],
					_.html('pp~fio', _, d),
				]]
			]],
			
			["li",{class: 'logout'},[
				["a",{id: 'logoutBtn', "href": "#"},[
					["i",{"class": "fas fa-sign-out-alt"}],
					["span",{"text": "Выйти"}],
					_.script(()=>{
						$(document).off('click', '#logoutBtn');
						$(document).on('click', '#logoutBtn', function(e){
							wsSend({action: 'logout'});
						});
					}),
				]]
			]]
		]]
	]],
	
]};