
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['phone'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
	
		data.type = 'input';
	
	return [

		window.el['__tpl~el_input'].tpl.bind(this)(_, d, data, tpl),

	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent)
		{
			if(data.value != undefined) data.value = '+7'+data.value;
			
			doAfterLoad.push(function(){
				realParent.find('input[code='+data.code+']').mask('+7(000)000-00-00');
			});
			
			this.save = function($e, data, callback){
				data.value = data.value ? data.value.replace(/\+7/, '').replace(/\D/g,'') : '';
				callback( data.value.length == 10 ? true : {err: 'Номер указан неверно'}, data );
			}
			if(!data.onSave){
				const flatTpl = tpl.flat(Infinity);
				const el = flatTpl[flatTpl.indexOf('input')+1];
				if(!el.onSave) el.onSave = 'saveTheme';
			}
		}
	},
}

exports['phone+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl~el_phone'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_phone'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	}
}

exports['phone-'] = 
exports['phone--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
	
		if(data.value != undefined) data.value = '+7'+data.value;
	
	return [
	
		(data.config||{}).callLink
			
			? ['a', {href: 'callto:'+data.value, text: data.value}]

			: window.el['__tpl~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_phone'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	}
}

exports['phone_block'] = {
	config: {
		customType: 'html',
		customName: true,
	},
	tpl: function(_, d, c){
		if(!c) c = {};
	return [
		["div",{"class": (c.class||'')+" form-group"},[
			["label",{},[
				["span",{"text": c.label || "Телефоны"}]
			]],
			["div",{"class": "row inline-complex"},[
				_.c(Object.assign({col: 'phone', link: '__phone', add: (_.editMode ? {label: 'Добавить телефон'} : false), process: {
					tpl: (_, d)=>[
						['div', {class: 'col-xs-4'}, [
							_.f({name: 'num', type: 'phone-', value: ''}),
						]],
					],
				}}, c.c||{})),
			]],
		]],
	]},
}

exports.action = {
	install: true,
	f: function(conn, data, callback){
		switch(data.type){
			
			case 'prepareContent':
				
				DB.createColKeys(conn, {col: 'phone', fields: ['num|int'], links: [], force: true}, (msg)=>{
					console.log(msg);
					callback();
				});
				break;
			
			default: callback(); break;
		}
	}
}