
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['email'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
	
		data.type = 'input';
	
	return [

		window.el['__tpl~el_input'].tpl.bind(this)(_, d, data, tpl),

	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent)
		{
			this.save = function($e, data, callback){
				callback( data.value && /^.+@.+\..+$/.test(data.value) === true ? true : {err: 'E-mail введен с ошибкой'}, data );
			}
			if(!data.onSave){
				const flatTpl = tpl.flat(Infinity);
				const el = flatTpl[flatTpl.indexOf('input')+1];
				if(!el.onSave) el.onSave = 'saveTheme';
			}
		}
	},
}

exports['email+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl~el_email'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_email'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	}
}

exports['email-'] = 
exports['email--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_email'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	}
}

exports['email_block'] = {
	config: {
		customType: 'html',
		customName: true,
	},
	tpl: function(_, d, c){
		if(!c) c = {};
	return [
		["div",{"class": (c.class||'')+" form-group"},[
			["label",{},[
				["span",{"text": c.label || "Электронная почта"}]
			]],
			["div",{"class": "row inline-complex"},[
				_.c(Object.assign({col: 'email', link: '__email', add: (_.editMode ? {label: 'Добавить почту'} : false), process: {
					tpl: (_, d)=>[
						['div', {class: 'col-xs-4'}, [
							_.f({name: 'mail', type: 'email-', value: ''}),
						]],
					],
				}}, c.c||{})),
			]],
		]],
	]},
}

exports.action = {
	install: true,
	f: function(conn, data, callback){
		switch(data.type){
			
			case 'prepareContent':
				
				DB.createColKeys(conn, {col: 'email', fields: ['mail|int'], links: [], force: true}, (msg)=>{
					console.log(msg);
					callback();
				});
				break;
			
			default: callback(); break;
		}
	}
}