
exports.config = {
	multi: true,
	//customName: true
}

exports['html'] = {
	/*config: {
		customType: 'html',
	},*/
	tpl: (_, d, c)=>{ return [
		["div",{"class": "ibox float-e-margins"},[
			["div",{"class": "ibox-title "+(c.titleClass||'')},[
				["h"+(c.titleSize||5),{},[
					["span",{"text": c.title||''}]
				]],
				['div', {class: 'ibox-tools'},[
					['a', {class: 'collapse-link'},[
						['i', {class: "fa fa-chevron-"+(c.hide?'down':'up')}],
					]],
					/*['a', {class: 'dropdown-toggle', "data-toggle":"dropdown", "href":"#", "aria-expanded":"false"},[
						['i', {class: "fa fa-wrench"}],
					]],
					['ul', {class: "dropdown-menu dropdown-user", "x-placement":"bottom-start"},[
						['li', {"href": "#", class:"dropdown-item", text: 'Config option 1'}],
					]],*/
				]],
			]],
			["div",{"class": "ibox-content "+(c.contentClass||''), style: c.hide ? 'display: none;' : ''},[
				c.content(_, d),
			]],
		]],
	]},
}