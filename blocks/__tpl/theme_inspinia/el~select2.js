
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['select2'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){

		data.type = 'select2';
		
	return [
		
		['div',{class: data.class+" form-group"},[
			['label',{},[
				['span',{text: data.label}],
			]],
			['select', Object.assign({}, data, {class: 'form-control'}), [
				this.element = function(e){ return [
					['option', {text: e.l, value: e.v, selected: data.multiple ? (data.value||[]).indexOf(e.v) != -1 : e.v == data.value}]
				]},
			]],
		]],

	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_select2'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['select2+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_select2'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl/theme_inspinia~el_select2'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['select2-'] = 
exports['select2--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]}
}