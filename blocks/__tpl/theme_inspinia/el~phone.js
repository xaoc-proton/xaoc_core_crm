
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['phone'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
	
		data.type = 'input';
	
	return [

		window.el['__tpl/theme_inspinia~el_input'].tpl.bind(this)(_, d, data, tpl),

	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_phone'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['phone+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_phone'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl/theme_inspinia~el_phone'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	}
}

exports['phone-'] = 
exports['phone--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
	
		if(data.value != undefined) data.value = '+7'+data.value;

	return [
		
		(data.config||{}).callLink
			
			? ['a', {href: 'callto:'+data.value, text: data.value}]
			
			: window.el['__tpl/theme_inspinia~el_label'].tpl.bind(this)(_, d, data, tpl)
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl/theme_inspinia~el_phone'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	}
}