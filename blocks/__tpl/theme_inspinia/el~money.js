
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['money'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
	
		data.type = 'input';
	
	return [

		window.el['__tpl/theme_inspinia~el_input'].tpl.bind(this)(_, d, data, tpl),

	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl~el_money'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	},
}

exports['money+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_inspinia~el_money'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl/theme_inspinia~el_money'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	}
}

exports['money-'] = 
exports['money--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){
	
		if(data.value != undefined) data.value = toMoney(data.value);
	
	return [

		window.el['__tpl/theme_inspinia~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]},
	front: {
		prepare: function(tpl, data, doAfterLoad, realParent){
			window.el['__tpl/theme_inspinia~el_money'].prepare.bind(this)(tpl, data, doAfterLoad, realParent);
		}
	}
}