
exports.config = {
	multi: true,
	customName: true,
}

exports['shelter'] = {
	config: {
		customType: 'html',
	},
	tpl: (_, d, c)=>{ return [
		['div', {class: 'shelter closed'}, [
			['label', {class: 'h', text: c.title||'Открыть блок'}],
			['div', {}, [
				c.content(_, d),
			]],
		]],
	]},
	script: ()=>{
		//$(document).off('click', '.shelter > label');
		$(document).on('click', '.shelter > label', function(){
			var $block = $(this).closest('.shelter');
			$block.toggleClass('closed');
			if(window.resizeAllTextarea) window.resizeAllTextarea( $block );
		});
	},
	 style: ()=>{/*
		.shelter > div::before {
			content: attr(itemcount);
		}
		.shelter:not(.closed) > div::before {
			display: none;
		}
		.shelter.closed > div > * {
			display: none;
		}
		.shelter.closed > label {
			color: #1c84c6;
		}
		.shelter .control-add {
			left: auto!important;
			right: 0px!important;
		}
	*/},
}