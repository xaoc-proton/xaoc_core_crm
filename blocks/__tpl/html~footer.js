exports.tpl = (_, d)=>{ return [

	["div",{"class": "footer *css*", style: ()=>{/*css
	
		@media (max-width: 768px){
			.*css* * {
				font-size: 12px;
			}
		}

	css*/}},[

		["div",{"class": "pull-right"},[
			["a",{"href": ""},[
				["span",{"text": "Политика обработки персональных данных"}]
			]]
		]],
		["div",{},[
			["span",{text:'© '+PROJECT+', '+moment().year()}]
		]]
	]]
]};