
exports.config = {
	multi: true,
	customName: true,
}

exports['form'] = {
	config: {
		customType: 'html',
	},
	tpl: (_, d, c)=>{ return [
		['div', {class: 'container-fluid flex-grow-1 p-0'}, [
			['div', {class: 'layout-content-header full pt-4 my-3'}, [
				['div', {class: 'layout-content-header__header'}, [
					['h1'],
				]],
			]],
			['div', {class: 'layout-content-wrapper'}, [
				['div', {class: 'layout-content-content'}, [
					c.content(_, d),
				]],
				['div', {class: 'hidden layout-content-bar'}, [
				]],
			]],
		]],
	]},
	script: ()=>{
		
	},
	style: ()=>{/*
		.layout-content-wrapper .row {
			margin-left: 0px;
			margin-right: 0px;
		}
	*/},
}