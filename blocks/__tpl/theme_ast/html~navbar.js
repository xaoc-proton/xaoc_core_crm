
exports.tpl = (_, d)=>{ return [

	["div",{class: "layout-navbar pb-0"},[
		["div",{class: "layout-navbar-search"},[
			["div",{class: "sh-search"},[
				["div",{class: "sh-search__tags"},[
					["span",{class: "badge sh-search__tags__tag badge-default"},[
						["span",{text: "Расчёты"}],
						["div",{class: "sh-icon sh-icon-close"},[
							["div",{class: "sh-icon-wraper",style: "width: 8px; height: 8px;"},[
								["svg",{width: "16",height: "16",viewBox: "0 0 16 16",fill: "none",xmlns: "http://www.w3.org/2000/svg",class: ""},[
									["path",{d: "M1.82091 0.312419C1.40435 -0.10414 0.728979 -0.10414 0.312419 0.312419C-0.10414 0.728979 -0.10414 1.40435 0.312419 1.82091L6.49151 8L0.31242 14.1791C-0.10414 14.5956 -0.10414 15.271 0.312419 15.6876C0.728979 16.1041 1.40435 16.1041 1.82091 15.6876L8 9.50849L14.1791 15.6876C14.5956 16.1041 15.271 16.1041 15.6876 15.6876C16.1041 15.271 16.1041 14.5956 15.6876 14.1791L9.50849 8L15.6876 1.82091C16.1041 1.40436 16.1041 0.728979 15.6876 0.31242C15.271 -0.10414 14.5956 -0.10414 14.1791 0.31242L8 6.49151L1.82091 0.312419Z",fill: "#949899",class: "fill"}]
								]]
							]]
						]]
					]]
				]],
				["input",{type: "text",placeholder: "Искать по расчётам",class: "sh-search__input form-control",id: "__BVID__121"}],
				["div",{class: "sh-search__icon sh-icon sh-icon-search"},[
					["div",{class: "sh-icon-wraper",style: "width: 16px; height: 16px;"},[
						["svg",{width: "20",height: "20",viewBox: "0 0 20 20",fill: "none",xmlns: "http://www.w3.org/2000/svg",class: ""},[
							["path",{d: "M19.3359 18.2109L14.7344 13.6094C15.875 12.2188 16.5625 10.4375 16.5625 8.49609C16.5625 4.04297 12.9492 0.429688 8.49609 0.429688C4.03906 0.429688 0.429688 4.04297 0.429688 8.49609C0.429688 12.9492 4.03906 16.5625 8.49609 16.5625C10.4375 16.5625 12.2148 15.8789 13.6055 14.7383L18.207 19.3359C18.5195 19.6484 19.0234 19.6484 19.3359 19.3359C19.6484 19.0273 19.6484 18.5195 19.3359 18.2109ZM8.49609 14.957C4.92969 14.957 2.03125 12.0586 2.03125 8.49609C2.03125 4.93359 4.92969 2.03125 8.49609 2.03125C12.0586 2.03125 14.9609 4.93359 14.9609 8.49609C14.9609 12.0586 12.0586 14.957 8.49609 14.957Z",fill: "#949899",class: "fill"}]
						]]
					]]
				]]
			]],
		]],
		["div",{class: "layout-navbar-profile"},[
			["div",{class: "sh-profile"},[
				["div",{class: "btn btn-icon-default tooltip-parent"},[
					["div",{class: "sh-icon sh-icon-clock hovered"},[
						["div",{class: "sh-icon-wraper filled",style: "width: 20px; height: 20px;"},[
							["svg",{width: "20",height: "20",viewBox: "0 0 20 20",fill: "none",xmlns: "http://www.w3.org/2000/svg",class: ""},[
								["path",{"fill-rule": "evenodd","clip-rule": "evenodd",d: "M19 10C19 14.9585 14.9741 19 10 19C5.02591 19 1 14.9585 1 10C1 5.02591 5.04145 1 10 1C14.9585 1 19 5.02591 19 10ZM12.5742 11.0388H9.58195C9.22536 11.0388 8.94629 10.7597 8.94629 10.4031V6.07751C8.94629 5.72092 9.22536 5.44185 9.58195 5.44185C9.93854 5.44185 10.2176 5.72092 10.2176 6.07751V9.76743H12.5742C12.9308 9.76743 13.2099 10.0465 13.2099 10.4031C13.2099 10.7597 12.9308 11.0388 12.5742 11.0388Z",fill: "#1774E2"}]
							]]
						]],
						["div",{class: "sh-icon-wraper",style: "width: 20px; height: 20px; margin-left: -20px;"},[
							["svg",{width: "20",height: "20",viewBox: "0 0 20 20",fill: "none",xmlns: "http://www.w3.org/2000/svg",class: ""},[
								["path",{d: "M9.99994 18.9612C14.9612 18.9612 18.9767 14.9302 18.9767 9.9845C18.9767 5.02326 14.9457 1.00775 9.99994 1.00775C5.0542 1.00775 1.02319 5.02326 1.02319 9.9845C1.02319 14.9302 5.0387 18.9612 9.99994 18.9612ZM9.99994 2.26357C14.2635 2.26357 17.7209 5.72093 17.7209 9.9845C17.7209 14.2481 14.2635 17.7054 9.99994 17.7054C5.73637 17.7054 2.27901 14.2326 2.27901 9.9845C2.27901 5.72093 5.73637 2.26357 9.99994 2.26357Z",fill: "#949899",class: "fill"}],
								["path",{d: "M9.58146 11.0387H12.5737C12.9303 11.0387 13.2094 10.7597 13.2094 10.4031C13.2094 10.0465 12.9303 9.76743 12.5737 9.76743H10.2171V6.07751C10.2171 5.72092 9.93805 5.44185 9.58146 5.44185C9.22487 5.44185 8.9458 5.72092 8.9458 6.07751V10.4031C8.9458 10.7597 9.22487 11.0387 9.58146 11.0387Z",fill: "#949899",class: "fill"}]
							]]
						]]
					]],
					["div",{
						role: "tooltip",tabindex: "-1", "x-placement": "bottom",
						class: "popover b-popover bs-popover-bottom sh-profile__popover hidden"+`css
							position: absolute; 
							right: 0px;
							left: auto;
							top: 30px;
							will-change: transform;
							max-width: 500px;
						`,
						//style: "position: absolute; transform: translate3d(583px, 48px, 0px); top: 0px; left: 0px; will-change: transform;"
					},[
						["div",{class: "arrow",style: "left: 197px;"}],
						["div",{class: "popover-body"},[
							["h3",{class: "sh-profile__popover__header"},[
								["span",{text: "История действий"}]
							]],
							["div",{class: "sh-profile__popover__content"},[
								["div",{class: "ps",style: "max-height: 200px;"},[
									["div",{class: "sh-profile__popover__content__item"},[
										["div",{class: "row"},[
											["div",{class: "col"},[
												["span",{text: "Раздел «Отчёт по продажам МБЦ»"}]
											]],
											["div",{class: "col-auto label"},[
												["span",{text: "30 секунд назад"}]
											]]
										]]
									]],
									["div",{class: "sh-profile__popover__content__item"},[
										["div",{class: "row"},[
											["div",{class: "col"},[
												["span",{text: "Новый расчёт #113199"}]
											]],
											["div",{class: "col-auto label"},[
												["span",{text: "2 минуты назад"}]
											]]
										]]
									]],
									["div",{class: "sh-profile__popover__content__item"},[
										["div",{class: "row"},[
											["div",{class: "col"},[
												["span",{text: "Раздел «Отчёт по пролонгации»"}]
											]],
											["div",{class: "col-auto label"},[
												["span",{text: "15 минут назад"}]
											]]
										]]
									]],
									["div",{class: "ps__rail-x",style: "left: 0px; bottom: 0px;"},[
										["div",{class: "ps__thumb-x",tabindex: "0",style: "left: 0px; width: 0px;"}]
									]],
									["div",{class: "ps__rail-y",style: "top: 0px; right: 0px;"},[
										["div",{class: "ps__thumb-y",tabindex: "0",style: "top: 0px; height: 0px;"}]
									]]
								]]
							]]
						]]
					]]
				]],
				["div",{class: "btn ml-2 btn-icon-default tooltip-parent "},[
					["span",{class: "badge badge-primary"},[
						["span",{text: "8"}]
					]],
					["div",{class: "sh-icon sh-icon-bell hovered"},[
						["div",{class: "sh-icon-wraper filled",style: "width: 20px; height: 20px;"},[
							["svg",{width: "20",height: "20",viewBox: "0 0 20 20",fill: "none",xmlns: "http://www.w3.org/2000/svg",class: ""},[
								["path",{d: "M10.0129 20C8.28961 20 6.88794 18.5983 6.88794 16.875C6.88794 16.53 7.16794 16.25 7.51294 16.25C8.68645 16.25 8.13517 16.25 10.0129 16.25C11.8908 16.25 11.3394 16.25 12.5129 16.25C12.8579 16.25 13.1379 16.53 13.1379 16.875C13.1379 18.5983 11.7363 20 10.0129 20Z",fill: "#1774E2"}],
								["path",{d: "M16.888 17.5H3.13797C2.33383 17.5 1.67969 16.8459 1.67969 16.0417C1.67969 15.6149 1.86554 15.2109 2.18964 14.9333C3.45718 13.8625 4.17969 12.3059 4.17969 10.6567V8.33328C4.17969 5.11673 6.79642 2.5 10.013 2.5C13.2297 2.5 15.8464 5.11673 15.8464 8.33328V10.6567C15.8464 12.3059 16.5689 13.8625 17.8281 14.9275C18.1606 15.2109 18.3464 15.6149 18.3464 16.0417C18.3464 16.8459 17.6923 17.5 16.888 17.5Z",fill: "#1774E2"}],
								["path",{d: "M10.0129 3.75C9.66794 3.75 9.38794 3.47 9.38794 3.125V0.625C9.38794 0.279999 9.66794 0 10.0129 0C10.3579 0 10.6379 0.279999 10.6379 0.625V3.125C10.6379 3.47 10.3579 3.75 10.0129 3.75Z",fill: "#1774E2"}]
							]]
						]],
						["div",{class: "sh-icon-wraper",style: "width: 20px; height: 20px; margin-left: -20px;"},[
							["svg",{width: "20",height: "20",viewBox: "0 0 20 20",fill: "none",xmlns: "http://www.w3.org/2000/svg",class: ""},[
								["path",{d: "M10.0129 20C8.28961 20 6.88794 18.5983 6.88794 16.875C6.88794 16.53 7.16794 16.25 7.51294 16.25C7.85794 16.25 8.13794 16.53 8.13794 16.875C8.13794 17.9092 8.97885 18.75 10.0129 18.75C11.0472 18.75 11.8879 17.9092 11.8879 16.875C11.8879 16.53 12.1679 16.25 12.5129 16.25C12.8579 16.25 13.1379 16.53 13.1379 16.875C13.1379 18.5983 11.7363 20 10.0129 20Z",fill: "#949899",class: "fill"}],
								["path",{d: "M16.888 17.5H3.13797C2.33383 17.5 1.67969 16.8459 1.67969 16.0417C1.67969 15.6149 1.86554 15.2109 2.18964 14.9333C3.45718 13.8625 4.17969 12.3059 4.17969 10.6567V8.33328C4.17969 5.11673 6.79642 2.5 10.013 2.5C13.2297 2.5 15.8464 5.11673 15.8464 8.33328V10.6567C15.8464 12.3059 16.5689 13.8625 17.8281 14.9275C18.1606 15.2109 18.3464 15.6149 18.3464 16.0417C18.3464 16.8459 17.6923 17.5 16.888 17.5ZM10.013 3.75C7.4855 3.75 5.42969 5.80582 5.42969 8.33328V10.6567C5.42969 12.6741 4.54559 14.5792 3.00476 15.8817C2.97546 15.9067 2.92969 15.9584 2.92969 16.0417C2.92969 16.1549 3.02475 16.25 3.13797 16.25H16.888C17.0013 16.25 17.0964 16.1549 17.0964 16.0417C17.0964 15.9584 17.0505 15.9067 17.023 15.8833C15.4805 14.5792 14.5964 12.6741 14.5964 10.6567V8.33328C14.5964 5.80582 12.5406 3.75 10.013 3.75Z",fill: "#949899",class: "fill"}],
								["path",{d: "M10.0129 3.75C9.66794 3.75 9.38794 3.47 9.38794 3.125V0.625C9.38794 0.279999 9.66794 0 10.0129 0C10.3579 0 10.6379 0.279999 10.6379 0.625V3.125C10.6379 3.47 10.3579 3.75 10.0129 3.75Z",fill: "#949899",class: "fill"}]
							]]
						]]
					]],

					["div",{
						role: "tooltip",tabindex: "-1","x-placement": "bottom",
						class: "popover b-popover bs-popover-bottom sh-profile__popover hidden"+`css
							position: absolute; 
							right: 0px;
							left: auto;
							top: 30px;
							will-change: transform;
							max-width: 500px;
						`,
					},[
						["div",{class: "arrow",style: "left: 244px;"}],
						["div",{class: "popover-body"},[
							["div",{class: "sh-profile__popover"},[
								["div",{class: "sh-profile__popover__header"},[
									["div",{class: "row"},[
										["div",{class: "col"},[
											["span",{text: "У вас 8 новых уведомлений"}]
										]],
										["div",{class: "col-auto"},[
											["button",{type: "button",class: "btn py-0 btn-link btn-sm"},[
												["span",{text: "Очистить все"}]
											]]
										]]
									]]
								]],
								["div",{class: "sh-profile__popover__content"},[
									["div",{class: "ps",style: "max-height: 200px;"},[
										["div",{class: "sh-profile__popover__content__item"},[
											["div",{class: "row"},[
												["div",{class: "col"},[
													["i",{class: "bg-primary shCircle mr-2"}],
													["span",{text: "Обновление системы"}]
												]],
												["div",{class: "col-auto label"},[
													["span",{text: "2 минуты назад"}]
												]]
											]],
											["p",{class: "label pt-2 mb-0"},[
												["span",{text: "Сегодня, в 12:00 будет произведено очередное обновление системы. Просим всех завершить свои задачи."}]
											]]
										]],
										["div",{class: "sh-profile__popover__content__item"},[
											["div",{class: "row"},[
												["div",{class: "col"},[
													["i",{class: "bg-primary shCircle mr-2"}],
													["span",{text: "Ответ по андеррайтингу #113143"}]
												]],
												["div",{class: "col-auto label"},[
													["span",{text: "Сегодня, 9:00"}]
												]]
											]]
										]],
										["div",{class: "sh-profile__popover__content__item"},[
											["div",{class: "row"},[
												["div",{class: "col"},[
													["span",{text: "Задача на расчёт #125302"}]
												]],
												["div",{class: "col-auto label"},[
													["span",{text: "26.03.2019, 09:17"}]
												]]
											]]
										]],
										["div",{class: "ps__rail-x",style: "left: 0px; bottom: 0px;"},[
											["div",{class: "ps__thumb-x",tabindex: "0",style: "left: 0px; width: 0px;"}]
										]],
										["div",{class: "ps__rail-y",style: "top: 0px; right: 0px;"},[
											["div",{class: "ps__thumb-y",tabindex: "0",style: "top: 0px; height: 0px;"}]
										]]
									]]
								]],
								["div",{class: "sh-profile__popover__footer"},[
									["button",{type: "button",class: "btn py-3 btn-link btn-sm btn-block"},[
										["span",{text: "Посмотреть все уведомления"}]
									]]
								]]
							]]
						]]
					]]

				]],
				["div",{class: "sh-profile-user tooltip-parent"},[
					_.c({name: 'pp', add: false, process: {
						tpl: (_, d)=>{ return [
							["div",{class: "d-flex align-self-start"},[
								_.f({name: 'foto', type: 'json'}),
								["img",{src: (d.foto||{}).l || '/blocks/pp/static/img/icon.png',class: "sh-profile-user-avatar"}]
							]],
							["div",{class: "sh-profile-user-info"},[
								["p",{class: "name mb-0"},[
									_.f({name: 'second_name', type: 'json'}),
									_.f({name: 'first_name', type: 'json'}),
									_.f({name: 'third_name', type: 'json'}),
									['span', {text: (d.second_name||'')+'\xa0'+(d.first_name||'')+'\xa0'+(d.third_name||'')}],
								]],
								["div",{class: "label mb-0"},[
									["div",{class: "dropdown *css*", style: ()=>{/*css
										.*css*.open > .dropdown-menu {
											display: flex;
											max-height: 400px;
											overflow: auto;
										}
										.*css* {
											white-space: nowrap;
										}
										.*css* > a {
											text-align: right;
										}
										.*css* > .dropdown-menu > div {
											padding: 10px;
										}
										.*css* > .dropdown-menu > div > div {
											padding: 8px 4px;
										}
										@media (max-width: 768px){
											.*css* > .dropdown-menu {
												width: 100%;
											}
										}
									css*/}},[
										_.c({name: 'user', add: false, process: {
											tpl: (_, d)=>[
												_.html('user~roles', _, d),
											],
										}}),
									]],
								]]
							]],
						]},
					}}),
					["div",{
						role: "tooltip", tabindex: "-1", "x-placement": "bottom",
						class: "popover b-popover bs-popover-bottom sh-profile__popover profile hidden"+`css
							position: absolute; 
							right: 0px;
							left: auto;
							top: 30px;
							will-change: transform;
						`},[
						["div",{class: "arrow",style: "left: 115px;"}],
						["div",{class: "popover-body"},[
							["div",{class: "sh-profile__popover"},[
								["div",{class: "sh-profile__popover__content"},[
									["div",{class: "sh-profile__popover__content__item"},[
										["a",{"href": "#", query: '{"form":"user~profile", "container":"formContent"}', class: "btn text-left btn-text btn-block"},[
											["span",{text: "Мой профиль"}]
										]]
									]],
									["div",{class: "sh-profile__popover__content__item"},[
										["button",{type: "button",class: "btn text-left btn-text btn-block"},[
											["span",{text: "Техническая поддержка"}]
										]]
									]],
									["div",{class: "sh-profile__popover__content__item"},[
										["button",{id: 'logoutBtn', type: "button",class: "btn text-left btn-text btn-block"},[
											["span",{text: "Выход"}],
											_.script(()=>{
												$(document).off('click', '#logoutBtn');
												$(document).on('click', '#logoutBtn', function(e){
													wsSend({action: 'logout'});
												});
											}),
										]]
									]]
								]]
							]]
						]]
					]]

				]]
			]]
		]]
	]],
	
]};
exports.script = ()=>{

}