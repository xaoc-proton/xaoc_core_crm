
exports.config = {
	multi: true,
	customName: (key)=>'el_'+key,
}

exports['label'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl~el_label'].tpl.bind(this)(_, d, data, tpl),

	]},
}

exports['label+'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_ast~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]},
}

exports['label-'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_ast~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]},
}

exports['label--'] = {
	config: {
		customType: 'html',
	},
	tpl: function(_, d, data, tpl){ return [

		window.el['__tpl/theme_ast~el_label'].tpl.bind(this)(_, d, data, tpl),
		
	]},
}