exports.tpl = (_, d)=>{
	
	const svg = ["svg",{width: "24",height: "24",viewBox: "0 0 24 24",fill: "none",xmlns: "http://www.w3.org/2000/svg",class: ""},[
		["rect",{x: "12",y: "20",width: "8",height: "1",rx: "0.5",class: "fill"}],
		["rect",{x: "12",y: "16",width: "12",height: "2",rx: "1",class: "fill"}],
		["circle",{cx: "4",cy: "18",r: "3","stroke-width": "2",class: "stroke"}],
		["rect",{x: "12",y: "8",width: "8",height: "1",rx: "0.5",class: "fill"}],
		["rect",{x: "12",y: "4",width: "12",height: "2",rx: "1",class: "fill"}],
		["circle",{cx: "4",cy: "6",r: "3","stroke-width": "2",class: "stroke"}]
	]];

return [

	["nav",{class: "sidenav layout-sidenav sidenav-vertical"},[
		["div",{class: "layout-sidenav-toggle"},[
			["div",{class: "sh-icon sh-icon-burger"},[
				["div",{class: "sh-icon-wraper",style: "width: 20px; height: 14px;"},[
					["svg",{width: "20",height: "14",viewBox: "0 0 20 14",fill: "none",xmlns: "http://www.w3.org/2000/svg",class: ""},[
						["g",{},[
							["rect",{width: "20",height: "2",class: "fill"}],
							["rect",{y: "6",width: "20",height: "2",class: "fill"}],
							["rect",{y: "12",width: "20",height: "2",class: "fill"}]
						]]
					]]
				]]
			]]
		]],
		["div",{class: "app-brand"},[
			["a",{href: "/",class: "app-brand-logo nuxt-link-active"},[
				["img",{src: "/img/lab-logo.png",class: "img-fluid"}]
			]]
		]],
		["div",{class: "sidenav-divider"}],
		["div",{class: "sidenav-inner ps"},[
			((_, d)=>
			{
				const admin = SYS.get(_.__.user, 'access.admin');
				return Object.keys(BLOCK.form)
					.filter(key => BLOCK.form[key].menu)
					.filter(key => SYS.get(_.__.user, 'access.'+key.split('~')[0]) || admin )
					.map(key=>
					{
						const item = BLOCK.form[key].menu;
						return ["div",{class: "level0"},[
							["div",{class: "sidenav-item","active-class": "active"},[
								["a",{
									class: "sidenav-link", "href": "#", 
									query: '{"form":"'+key.replace('.js','')+'", "container":"formContent"}'
								},[
									//!item.icon ? [] : ["i",{"class": item.icon}],
									["div",{class: "level0-icon sh-icon sh-icon-ic_document"},[
										["div",{class: "sh-icon-wraper",style: "width: 20px; height: 20px;"},[
											svg
										]]
									]],
									["div",{text: item.label || key}],
								]],
							]],
						]]
					})
			})(_, d),			
		]],
	]],
]}

exports.script = ()=>{
	$(document)
		.off('click', '.layout-sidenav-toggle')
		.on('click', '.layout-sidenav-toggle', function(){
			$('.sidenav, .layout-container').toggleClass('toggle');
			$('html').toggleClass('layout-collapsed');
			$('.app-brand-logo > img').toggleClass('logo-small');
		});
}

exports.func = ()=>{

	window.breadcrumb = [];
	
	window.updateMenu = function (data) {
		
		var info = {};
		
		if (typeof window.updateMenuCustom == "function") {
			
			window.updateMenuCustom(info);
			window.updateMenuCustom = undefined;
		}
		
		const $h1 = $('.layout-content-header__header > h1').first();
		
		if(info.text){
			$h1.text( info.text );
		}else{
			if(!$h1.text() && window.breadcrumb.length)
				$h1.text( window.breadcrumb[window.breadcrumb.length-1].text );
		}
	}
}