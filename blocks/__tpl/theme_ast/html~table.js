
exports.tpl = (_, d, c)=>{

	if(!c.filter) c.filter = {};
	if(!c.filter.items) c.filter.items = [];
	c.filter.items = c.filter.items.filter(_=>_);
	if(c.filter.items.length && !c.filter.findBtn) c.filter.findBtn = {};
	
	if(c.add === true) c.add = {modal: true};
	if(c.add){
		if(!c.add.btn) c.add.btn = {};
	}
	
	if(!c.table) c.table = {};
	if(!c.table.filter) c.table.filter = -10;
	if(!c.table.items) c.table.items = [];
	c.table.items = c.table.items.filter(_=>_);
	if(!c.table.itemLink) c.table.itemLink = {};
	if(!c.table.itemLink.type) c.table.itemLink.type = 'row';
	if(!c.table.itemLink.link) c.table.itemLink.link = {form: c.col+'~main', container: 'formContent'};

return [
	
	!(c.add||{}).modal ? [] : _.html('__tpl~modal', _, d, c),
	
	["div",{class: "layout-content-header full pt-4 my-3"},[
		["div",{class: "layout-content-header__header"},[
			["h1",{},[
				["span",{text: c.label||''}]
			]],
			!c.add ? [] : (c.add.btn.html || [
				_.if(_.editMode, ()=>[
					["button",{id: 'btn-new-'+c.col, "toggle-modal": "#"+c.col+"-modal", class: (c.add.btn.class || "btn ml-3 btn-primary")},[
						["span",{"text": c.add.btn.label || "Новый "+(c.label||'')}]
					]],
				]),
			]),
			/*["button",{type: "button",class: "btn ml-3 btn-primary"},[
				["span",{text: "Новый расчёт"}]
			]],
			["button",{type: "button",class: "btn ml-3 btn-outline"},[
				["span",{text: "Пролонгация"}]
			]]*/
		]],
		["div",{class: "layout-content-header__controls w-auto"},[
			["div",{class: "d-flex align-items-center"},[
				["p",{class: ""},[
					["span",{text: "Найдено 2038 расчётов"}]
				]],
				["button",{type: "button",class: "btn ml-3 btn-icon-default"},[
					["div",{class: "sh-icon sh-icon-filter"},[
						["div",{class: "sh-icon-wraper",style: "width: 20px; height: 20px;"},[
							["svg",{width: "20",height: "20",viewBox: "0 0 20 20",fill: "none",xmlns: "http://www.w3.org/2000/svg",class: ""},[
								["g",{"clip-path": "url(#clip0)"},[
									["path",{d: "M18.9248 -2.30875e-06H1.09382C0.779325 -0.000597187 0.491008 0.175089 0.347247 0.454879C0.201502 0.738437 0.227082 1.0799 0.413278 1.33886L6.9458 10.5412C6.94799 10.5444 6.95036 10.5473 6.95255 10.5505C7.1899 10.871 7.3182 11.259 7.31899 11.658V19.1605C7.3176 19.3828 7.40485 19.5962 7.5615 19.7538C7.71815 19.9115 7.93112 19.9999 8.1532 19.9999C8.26623 19.9999 8.37787 19.9773 8.48217 19.9341L12.1529 18.5343C12.4819 18.434 12.7002 18.1237 12.7002 17.7499V11.658C12.7008 11.2592 12.8293 10.871 13.0665 10.5505C13.0687 10.5473 13.071 10.5444 13.0732 10.5412L19.6055 1.33847C19.7917 1.0797 19.8173 0.738437 19.6716 0.454879C19.5278 0.175089 19.2395 -0.000597187 18.9248 -2.30875e-06ZM12.2479 9.94909C11.8831 10.4442 11.6858 11.0429 11.6846 11.658V17.6266L8.33424 18.904V11.658C8.33305 11.0429 8.13575 10.4442 7.7707 9.94909L1.42873 1.01525H18.5901L12.2479 9.94909Z",fill: "#949899",class: "fill"}]
								]],
								["defs",{},[
									["clipPath",{id: "clip0"},[
										["rect",{width: "20",height: "20",fill: "white"}]
									]]
								]]
							]]
						]]
					]]
				]]
			]]
		]]
	]],
	["div",{},[
		["div",{class: "b-overlay-wrap position-relative",_blur: "2px",_rounded: "sm"},[
			["div",{style: "position: relative;"},[
				["div",{class: "shCustomizableTable table-responsive"},[
					["table",{id: c.col+'-table', role: "table", class: "table b-table table-striped table-hover table-bordered"},[
						["thead",{role: "rowgroup",class: ""},[
							["tr",{role: "row",class: ""},[
								c.table.items
								.concat(c.table.itemLink.type != 'col' ? [] : {label: '', class: 'min'})
								.map(item=>
									["th",{class: item.class||'', "aria-sort":"none"},[
										["div",{text: item.label||""}],
									]],
								)
							]],
						]],
						_.html('__tpl~table_body', _, d, Object.assign(c, {
							tbody: {role: "rowgroup"},
							row: {content: (_, d)=>[
								_.config.table.prepareItem(_, d, 
									
									["tr", {
										role: "row",
										class: _.config.table.itemLink.type == 'row' ? 'h' : '',
										query: _.config.table.itemLink.type == 'row' ? _.config.table.itemLink.query : undefined,
									},[
										_.config.table.items.map(item=>["td",{role: "cell", class: item.class||''}, [
											["div",{class: "bCellSimple"},[]
											
												.concat(item.f ? [_.f( item.f )] : [])
												
												.concat(item.c ? [_.c( {name: item.c.name, add: false, config: {f: item.c.f}, process: {
													tpl: (_, d)=>(_.config.f ? [ _.f( _.config.f ) ] : [])
												}})] : [])
												
												.concat(item.html ? item.html(_, d) : [])
											]
										]]).concat(
											[_.config.table.itemLink.type != 'col' ? [] : ["td", {class: 'edit h', query: _.config.table.itemLink.query}]]
										),
									]],
								)
							]}
						})),
					]],
				]],
			]],
		]],
	]],
]};

exports.script = ()=>{

}

exports.style = ()=>{/*

*/}