

exports.config = {
	multi: true,
	customName: true,
}

exports['table'] = {
	config: {
		customType: 'html',
	},
	tpl: (_, d, c)=>{

		if(!c.filter) c.filter = {};
		if(!c.filter.items) c.filter.items = [];
		c.filter.items = c.filter.items.filter(_=>_);
		if(c.filter.items.length && !c.filter.findBtn) c.filter.findBtn = {};
		
		if(c.add === true) c.add = {modal: true};
		if(c.add){
			if(!c.add.btn) c.add.btn = {};
		}
		
		if(!c.table) c.table = {};
		if(!c.table.filter) c.table.filter = -10;
		if(!c.table.items) c.table.items = [];
		c.table.items = c.table.items.filter(_=>_);
		if(!c.table.itemLink) c.table.itemLink = {};
		if(!c.table.itemLink.type) c.table.itemLink.type = 'row';
		if(!c.table.itemLink.link) c.table.itemLink.link = {form: c.col+'~main', container: 'formContent'};

	return [
		
		!(c.add||{}).modal ? [] : _.html('__tpl~modal', _, d, c),
		
		["div",{"class": "bPageFilters"},[
			["div",{"class": "row"},[
				c.filter.items.map(item=>
					["div",{"class": item.class || "col-xs-3"},[
						_.f( Object.assign(item.f||{}, {stype: 'filter'}) ),
					]]
				)
				.concat(!c.filter.findBtn ? [] : (c.filter.findBtn.html || [
					["div",{"class": c.filter.findBtn.class||"col-xs-1"},[
						['div', {text: c.filter.findBtn.label||'Найти', class: 'btn btn-primary'}, [
							_.f({name: 'find_btn', type: 'action', front: {
								onClick: (_)=>{
									var filter = {};
									_.closest('.ibox').find('.filter-field').map(function(i,f){
										if(f.classList.contains("hasDatepicker")){
											filter[f.name] = f.value.replace( /(\d{2})\.(\d{2})\.(\d{4})/, "$3-$2-$1");
										}else{
											filter[f.name] = f.type == 'checkbox' ? f.checked : f.value;
										} 
									});
									filter.force = true;
									reloadComplex(_.closest('.ibox').find('tbody'), filter);
								}
							}}),
						]],
					]]
				]))
				.concat(!c.add ? [] : (c.add.btn.html || [
					_.if(_.editMode, ()=>[
						["div",{"class": c.add.btn.class || "text-right"},[
							["div",{id: 'btn-new-'+c.col, "class": "btn btn-primary","data-toggle": "modal","data-target": "#"+c.col+"-modal"},[
								["span",{"text": c.add.btn.label || "Новый "+(c.label||'')}]
							]]
						]],
					]),
				]))
			]],
		]],
		["div",{"class": "table-responsive"},[
			["table",{id: c.col+'-table', "class": "table table-hover table-striped table-bordered"},[
				["thead",{},[
					["tr",{},[
						( !c.table.checkItems ? [] : [
							["td",{"class": "min"},[
								_.f({name: 'checkAllItems', type: 'check', stype: 'filter', config: {content: false}, front: {
									onFilter: ($e)=>{
										$e.closest('table')
											.find('[name=checkItem]')
											.prop('checked', $e.prop('checked')?'checked':'')
											.trigger('change');
									}
								}}),
							]]
						]).concat(c.table.items
							.concat(c.table.itemLink.type != 'col' ? [] : {label: '', class: 'min'})
							.map(item=>
								["td",{class: item.class||''},[
									["span",{"text": item.label||""}],
								]],
							)
						)
					]]
				]],
				_.html('__tpl~table_body', _, d, Object.assign(c, {
					row: {content: (_, d)=>[
						_.config.table.prepareItem(_, d, 
							
							["tr", _.config.table.itemLink.type == 'row'  ? {class: 'h', query: _.config.table.itemLink.query} : {}, [
								
								( !_.config.table.checkItems ? [] : [
									["td",{itemId: d._id},[
										_.f({name: 'checkItem', type: 'check', stype: 'filter', config: {content: false}}),
									]]
								]).concat(
									_.config.table.items.map(item=>["td",{class: item.class||''},[]
										
										.concat(item.f ? [_.f( item.f )] : [])
										
										.concat(item.c ? [_.c( {name: item.c.name, add: false, config: {f: item.c.f}, process: {
											tpl: (_, d)=>(_.config.f ? [ _.f( _.config.f ) ] : [])
										}})] : [])
										
										.concat(item.html ? item.html(_, d) : [])
										
									])
								).concat(
									[_.config.table.itemLink.type != 'col' ? [] : ["td", {class: 'edit h', query: _.config.table.itemLink.query}]]
								),
							]],
						)
					]}
				})),
			]]
		]],
		["hr"],
		["div",{"class": "row m-top-lg form-inline"},[
			
		]]
		
	]},
	script: ()=>{
		
	},
	style: ()=>{/*
		.table .max100 { max-width: 100px }
		.table .max150 { max-width: 150px }
		.table .max200 { max-width: 200px }
	*/}
}

exports['table_body'] = {
	config: {
		customType: 'html',
	},
	tpl: (_, d, c)=>{ return [
		["tbody",Object.assign({class: `css
			.*css* > * > .item-controls {
				display: none!important;
			}
			.*css* .btn-lastitem {
				position: absolute;
				right: 20px;
				bottom: 10px;
			}
			.*css* .btn-lastitem > * {
				border: none;
				background-color: #fff;
				padding: 0px;
			}
			.*css* .edit {
				min-width: 40px;
				background-image: url(/XAOC/images/edit.png);
				background-size: 20px;
				background-repeat: no-repeat;
				background-position: center;
			}
			.*css* .deleted {
				background-color: #ccc!important;
			}
		`}, c.tbody||{}),[
			_.c({name: c.col, add: false, config: Object.assign(d.config||{}, c), filter: {l: c.table.filter}, controls: {show: {label: 'Показать следующие '+Math.abs(c.table.filter)}}, process: {
				// для корректной работы с hasParent у родителя-формы в exports.id должен быть прописан правильный __.fields[code].link
				parentDataNotRequired: c.hasParent ? false : true,
				id_prepare: SYS.get(c, 'table.id.prepare') || function(){},
				id: c.hasParent ? null : (__, code, callback)=>{ // другие значения кроме null где-то ломают маркап (внутри item пропадает _.config)
					
					var field = __.fields[code] || {};
					var config = field.config || {table: {id: {}}};
					var data = __.data[field.parent] || {};
					
					if(!config.table.id) config.table.id = {};
					if(!config.table.id.baseField) config.table.id.baseField = 'add_time';

					var select = {
						escape: [], join: [], where: [], order: [], 
						__: __, data: data, config: config, filter: field.filter,
						push: function(d){
							this.where.push(this.config.col+'__'+d[0]+'.f '+d[1]+' '+d[2]);
							if(d[2] == '?') this.escape.push(d[3]);
						}
					};

					select.sql = "SELECT "+config.col+"__"+config.table.id.baseField+".id _id FROM "+config.col+"__"+config.table.id.baseField;
					
					if(config.table.id.baseField == 'add_time'){
						select.where.push(config.col+"__"+config.table.id.baseField+".f > 0");
						select.order.push(config.col+"__"+config.table.id.baseField+".f");
					}
					(SYS.get(__.process[field.linecode], 'id_prepare'))(select);
					
					if(select.join.length) select.sql += " "+select.join.join(' ');
					if(select.where.length) select.sql += " WHERE "+select.where.join(' AND ');
					if(select.order.length) select.sql += " ORDER BY "+select.order.join(', ');

					DB.selectWithFilter(__, code, callback, select.sql, select.escape);
				},
				tpl: (_, d)=>{
					
					if(_.__.pre){
						if(_.config.table.prepareItem) _.__.process[_.linecode+'_table_item_prepare'] = {action: _.config.table.prepareItem};
						if(SYS.get(_.config, 'row.content')) _.__.process[_.linecode+'_table_row_content'] = {action: _.config.row.content};
					}else{
						if(_.__.process[_.linecode+'_table_item_prepare']) _.config.table.prepareItem = _.__.process[_.linecode+'_table_item_prepare'].action;
						if(_.__.process[_.linecode+'_table_row_content']) SYS.set(_.config, 'row.content', _.__.process[_.linecode+'_table_row_content'].action);
					}
					if(!_.config.table.prepareItem) _.config.table.prepareItem = (_, d, item)=>item;

					_.config.table.items.forEach((item, key)=>{
						if(_.__.pre){
							if(item.html) _.__.process[_.linecode+'_table_item_'+key] = {tpl: item.html};
						}else{
							if(_.__.process[_.linecode+'_table_item_'+key]) item.html = _.__.process[_.linecode+'_table_item_'+key].tpl;
						}
					});
					
					_.config.table.itemLink.query = JSON.stringify( Object.assign({filter: {id: d._id}}, _.config.table.itemLink.link || {}) );

				return [
					(SYS.get(_.config, 'row.content')||(()=>[]))(_, d),
				]},
			}}, {tag: 'tr'}),
		]]
	]},
}
	